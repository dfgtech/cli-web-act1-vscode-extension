// Valida el uso apropiado de la sintaxis del lenguaje de progración utilizado
'use strict';
// Se importan los componentes del API de VS Code extensibility para utilizarlos en la codificación de la extensión
import * as vscode from 'vscode';
import { isUndefined } from 'util';

// Función principal de la extensión que VS Code invocará una sola vez cuando cualquiera de los eventos "actionEvents" descritos en el archivo
// package.json se ejecutan
export function activate(context: vscode.ExtensionContext) {

    // Log de la consola que nos indicará el inicio de la ejecución de nuestra extensión VS Code
    console.log('Congratulations, your extension "gapline" is now active!');

    // Registro e implementación del comando definido en el archivo package.json
    // El commandId 'extension.gapline' debe coincidir con el declarado en el archivo package.json
    let disposable = vscode.commands.registerCommand('extension.gapline', () => {
        // Captura el editor de texto activo de Visual Studio Code
        var editor = vscode.window.activeTextEditor;
        // Se valida que el editor no sea indefinido para proceder con la ejecución de la extensión, caso contrario se para la ejecución
        if (isUndefined(editor) || !editor) {
            // Mensaje que indica la razón de la terminación de la ejecución
            console.log('Active Editor not found... Application Terminated!');
            return;
        }
        // Retorna y asigna una variables con el objeto que representa el texto seleccionado dentro del editor de texto activo
        var selection = editor.selection;
        // Devuelve el texto como un string para poder manipularlo dentro de la ejecución de la extensión
        var text = editor.document.getText(selection);
        // Despliega en el editor de donde se ejecutó la extensión un campo de texto donde se podrá ingresar el número de líneas a contar antes de colocar un espacio en blanco
        vscode.window.showInputBox({ prompt: "Lineas?" }).then(value => {
            // Se almacena el valor seleccionado por el usuario en una variable de tipo "number"
            let numberOfLines = isUndefined(value) ? 0 : +value;
            // Se inicializa un arreglo de cadenas de caracteres "strings" donde almacenará cada línea de texto
            var textInChunks: Array<string> = [];
            // Se ejecuta la función split dentro del texto seleccionado indicando que itere sobre cada línea de texto por cada salto de línea encontrado en el texto seleccionado
            text.split("\n").forEach((currentLine: string, lineIndex) => {
                // Se agrega la línea al arreglo previamente inicializado
                textInChunks.push(currentLine);
                // Si se cumple el conteo de número de líneas definida por el usuario se inserta una línea en blanco dentro del arreglo
                if (((lineIndex + 1) % numberOfLines) === 0) textInChunks.push("");
            });
            // Se vuelve a unir las líneas del arreglo en un solo texto con saltos de línea
            text = textInChunks.join("\n");
            // Se valida nuevamente que el editor no se indefinido
            if(!isUndefined(editor)){
                // Se invoca el API de VS Code para poder hacer una edición dentro del editor activo
                editor.edit(editBuilder => {
                    // Con este código se obtiene el rango previamente seleccionado de texto, con su inicio y fin definidos
                    var range = new vscode.Range(
                        selection.start.line,
                        0,
                        selection.end.line,
                        !isUndefined(editor) ? editor.document.lineAt(selection.end.line).text.length : 0
                    );
                    // Se reemplaza el nuevo texto dentro del rango definido obtenido previamente
                    editBuilder.replace(range, text);
                });
            }

        });
    });
    // Se suscribe la función al contexto de la aplicación para que escuche cualquier ejecución del actionEvent asociado al comando registrado
    context.subscriptions.push(disposable);
}

// Este método se invocará cuando la extensión sea desactivada y permite implementar cierta lógica de liberación de recursos
// especialmente si la extensión hace uso de recursos del sistema operativo como determinadas subrutinas o subprocesos
export function deactivate() {
}
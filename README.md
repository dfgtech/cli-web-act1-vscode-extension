## ***Nombre del Estudiante: Diego Flores***

# Actividad 1: Creación de una extensión para Visual Studio Code 

> __NOTA: EL DESARROLLO DE ESTE EJERCICIO FUE SOBRE UNA PLATAFORMA WINDOWS__

En este documento se describe la secuencia de pasos realizados para cumplir con la actividad propuesta.

## __1.__ __Instalación de NodeJS__ 

Para la correcta realización de esta actividad fue requerido instalar algunos programas y paquetes de software, uno de ellos es NodeJS, desde una plataforma Windows simplemente se debe descargar el instalador gratuito desde Internet y seguir el wizard de instalación. En el siguiente apartado se puede encontrar la guía de instalación de NodeJS: [Tutotial de instalación de NodeJS](https://nodejs.org/en/download/package-manager/#windows)

## __2.__ __Instalación de VS Code__

Otro programa fundamental para el desarrollo de la actividad es el poderoso editor de texto Visual Studio Code. En el siguiente apartado se puede encontrar la guía de instalación de VS Code: [Tutorial de instalación de VS Code](https://code.visualstudio.com/docs/setup/windows)

## __3.__ __Instalación de paquetes NPM requeridos para la creación de extensiones VS Code__

Una vez instalado NodeJS, si la configuración de la variable de entorno %PATH% fue modificada exitosamente, tendremos disponibles los binarios de NodeJS listos para utilizarse desde una consola/terminal windows.

Ejecutar el siguiente comando desde el terminal de windows:

```console
> npm install -g yo generator-code
```
## __4.__ __Generación del esqueleto del proyecto__ 

Debemos ubicarnos en el directorio donde se desea generar el nuevo proyecto de tipo "VS Code Extension" y ejecutar el siguiente comando:

```console
> yo code
```

Se nos desplegará un wizard de creación para el nuevo proyecto, similar al que se muestra a continuación:

![Wizard de Creación](images/wizard.PNG)

Debemos seleccionar las siguientes opciones:

1. Tipo de lenguaje de desarrollo: New Extension (TypeScript)
2. Nombre de extensión colocamos: Line Gapper
3. Identificador: gapline
4. Publisher Name: Omitir
5. Iniciar Repositorio Git: Omitir
6. _Demás opciones: Opción por defecto_

Todas estas configuraciones se guardan en el archivo package.json sobre la raíz del proyecto. Todos los demás ficheros forman parte de la generación del proyecto con Yeoman.

## __5.__ __Apertura del proyecto con VS Code__ 

Una vez generado el proyecto, se procede a abrir el folder principal con la misma herramienta VS Code. El mismo editor reconocerá el tipo de proyecto y posiblemente sugiera la instalación de algunos componentes adicionales o alguna actualización de software disponible de la herramienta. Se sugiere que se acepte las recomendaciones dadas por el editor y que se reinicie el mismo para que apliquen los cambios realizados después de la instalación.

## __6.__ __Código de la extensión__ 

El fichero principal que contiene la codificación de la extensión se encuentra en la siguiente ruta dentro del proyecto: `src/extension.ts`

Por defecto al generar el proyecto con Yeoman, este nos coloca un código genérico "Hola Mundo" dentro del archivo extension.ts. Por lo que se procede a editar este código fuente y reemplazarlo por el siguiente:

```typescript
'use strict';

import * as vscode from 'vscode';
import { isUndefined } from 'util';

export function activate(context: vscode.ExtensionContext) {

    console.log('Congratulations, your extension "gapline" is now active!');

    let disposable = vscode.commands.registerCommand('extension.gapline', () => {
        var editor = vscode.window.activeTextEditor;
        if (isUndefined(editor) || !editor) {
            console.log('Active Editor not found... Application Terminated!');
            return;
        }

        var selection = editor.selection;
        var text = editor.document.getText(selection);

        vscode.window.showInputBox({ prompt: "Lineas?" }).then(value => {
            let numberOfLines = isUndefined(value) ? 0 : +value;
            var textInChunks: Array<string> = [];
            text.split("\n").forEach((currentLine: string, lineIndex) => {
                textInChunks.push(currentLine);
                if (((lineIndex + 1) % numberOfLines) === 0) textInChunks.push("");
            });
            text = textInChunks.join("\n");
            if(!isUndefined(editor)){
                editor.edit(editBuilder => {
                    var range = new vscode.Range(
                        selection.start.line,
                        0,
                        selection.end.line,
                        !isUndefined(editor) ? editor.document.lineAt(selection.end.line).text.length : 0
                    );
                    editBuilder.replace(range, text);
                });
            }

        });
    });
    context.subscriptions.push(disposable);
}

export function deactivate() {
}
```
# Actividad 1: Desarrollo de los ejercicios

## _Primer Ejercicio_

> Comenta cada línea del código anterior teniendo en cuenta el propósito descrito al principio de este enunciado. Busca en el API de las extensiones de VS Code y averigua para qué sirve la función activate y deactivate y el resto de las funciones y propiedades específicas de las extensiones de VS Code.

```typescript
// Valida el uso apropiado de la sintaxis del lenguaje de progración utilizado
'use strict';
// Se importan los componentes del API de VS Code extensibility para utilizarlos en la codificación de la extensión
import * as vscode from 'vscode';
import { isUndefined } from 'util';

// Función principal de la extensión que VS Code invocará una sola vez cuando cualquiera de los eventos "actionEvents" descritos en el archivo
// package.json se ejecutan
export function activate(context: vscode.ExtensionContext) {

    // Log de la consola que nos indicará el inicio de la ejecución de nuestra extensión VS Code
    console.log('Congratulations, your extension "gapline" is now active!');

    // Registro e implementación del comando definido en el archivo package.json
    // El commandId 'extension.gapline' debe coincidir con el declarado en el archivo package.json
    let disposable = vscode.commands.registerCommand('extension.gapline', () => {
        // Captura el editor de texto activo de Visual Studio Code
        var editor = vscode.window.activeTextEditor;
        // Se valida que el editor no sea indefinido para proceder con la ejecución de la extensión, caso contrario se para la ejecución
        if (isUndefined(editor) || !editor) {
            // Mensaje que indica la razón de la terminación de la ejecución
            console.log('Active Editor not found... Application Terminated!');
            return;
        }
        // Retorna y asigna una variables con el objeto que representa el texto seleccionado dentro del editor de texto activo
        var selection = editor.selection;
        // Devuelve el texto como un string para poder manipularlo dentro de la ejecución de la extensión
        var text = editor.document.getText(selection);
        // Despliega en el editor de donde se ejecutó la extensión un campo de texto donde se podrá ingresar el número de líneas a contar antes de colocar un espacio en blanco
        vscode.window.showInputBox({ prompt: "Lineas?" }).then(value => {
            // Se almacena el valor seleccionado por el usuario en una variable de tipo "number"
            let numberOfLines = isUndefined(value) ? 0 : +value;
            // Se inicializa un arreglo de cadenas de caracteres "strings" donde almacenará cada línea de texto
            var textInChunks: Array<string> = [];
            // Se ejecuta la función split dentro del texto seleccionado indicando que itere sobre cada línea de texto por cada salto de línea encontrado en el texto seleccionado
            text.split("\n").forEach((currentLine: string, lineIndex) => {
                // Se agrega la línea al arreglo previamente inicializado
                textInChunks.push(currentLine);
                // Si se cumple el conteo de número de líneas definida por el usuario se inserta una línea en blanco dentro del arreglo
                if (((lineIndex + 1) % numberOfLines) === 0) textInChunks.push("");
            });
            // Se vuelve a unir las líneas del arreglo en un solo texto con saltos de línea
            text = textInChunks.join("\n");
            // Se valida nuevamente que el editor no se indefinido
            if(!isUndefined(editor)){
                // Se invoca el API de VS Code para poder hacer una edición dentro del editor activo
                editor.edit(editBuilder => {
                    // Con este código se obtiene el rango previamente seleccionado de texto, con su inicio y fin definidos
                    var range = new vscode.Range(
                        selection.start.line,
                        0,
                        selection.end.line,
                        !isUndefined(editor) ? editor.document.lineAt(selection.end.line).text.length : 0
                    );
                    // Se reemplaza el nuevo texto dentro del rango definido obtenido previamente
                    editBuilder.replace(range, text);
                });
            }

        });
    });
    // Se suscribe la función al contexto de la aplicación para que escuche cualquier ejecución del actionEvent asociado al comando registrado
    context.subscriptions.push(disposable);
}

// Este método se invocará cuando la extensión sea desactivada y permite implementar cierta lógica de liberación de recursos
// especialmente si la extensión hace uso de recursos del sistema operativo como determinadas subrutinas o subprocesos
export function deactivate() {
}
```

> Es importante que entiendas cada línea de código. Si tienes alguna dificultad, puedes preguntar en el foro ad hoc para la resolución del ejercicio. ¿Qué elementos específicos de TypeScript identificas? Recuerda que el profesor está para ayudarte y que esta ayuda continua en el foro.

Elemento | Descripción
------------ | -------------
`import` | Importa los componentes disponibles en \[node_modules\] necesarios que se utilizarán dentro del código typescript
`export function` | Declaración y firma que posee una función en typescript
`let` | Nuevo tipo de variable introducido para superar algunos problemas conocidos del uso de `var` en javascript
`(param1: type1, param2: type2) => { //implementacion }` | Uso de expresiones Lambda para la definición de funciones

## _Segundo Ejercicio_

> Comprueba que tu extensión funciona. Para ello, primero asegúrate de que sustituyes todos los sayHello (que inserta por defecto el generador code que hemos aplicado con Yeoman) por el nombre de la función que hemos usado (gapline, en nuestro caso) en el fichero package.json. Como hemos comentado antes, este fichero contiene las opciones e información básica de nuestra extensión y, entre otras cosas, cómo ejecutarlas. Es especialmente importante las secciones activationEvents y commands. Vigila que el nombre del comando sea el correcto.

> Para ejecutar la extensión, pulsa F5 (o la tecla función + F5, dependiendo de tu teclado y sistema) o selecciona la opción de menú Start debugging del menú Debug. Verás que se abre una nueva ventana de VS Code (que ya tiene tu extensión precargada).

> Abre un fichero de texto cualquiera, selecciona su contenido y pulsa control/comando + shift + P. Se abrirá el visor de comandos de VS Code (el command palette). Busca el nombre de tu extensión (tendrá el mismo nombre que hayas puesto en el campo Display Name en el fichero package.json o el que le diste cuando la creaste con el generador de Yeoman).

> Debería aparecer un nuevo cuadro de diálogo preguntándote cada cuántas líneas quieres insertar una línea en blanco (o el texto que hayas puesto en la propiedad prompt del código anterior). Selecciona un número apropiado y pulsa intro. Verás que el texto seleccionado es sustituido por uno nuevo pero que contiene líneas en blanco.

### Paso a Paso

### __1.__ __Cambiar el nombre del comando por defecto `sayHello` por `gapline`__

Debemos asegurarnos de que la referencia anterior al comando `sayHello` sea reemplazada por la actual definición de la extensión `gapline` que se registró a través del método `registerCommand` del archivo `extension.ts`. Adicional a esto se debe cambiar el nombre, descripción y título de la extensión que identifique propiamente la extensión que se desea usar más adelante. Para ello se debe editar el fichero `package.json` y debemos centrarnos en las secciones `activationEvents` y `commands` como se observa a continuación:

```json
{
    "name": "gapline",
    "displayName": "Line Gapper",
    "description": "Line Gapper Beta",
    "version": "0.0.1",
    "engines": {
        "vscode": "^1.29.0"
    },
    "categories": [
        "Other"
    ],
    "activationEvents": [
        "onCommand:extension.gapline"
    ],
    "main": "./out/extension",
    "contributes": {
        "commands": [
            {
                "command": "extension.gapline",
                "title": "Line Gapper"
            }
        ]
    },
    "scripts": {
        "vscode:prepublish": "npm run compile",
        "compile": "tsc -p ./",
        "watch": "tsc -watch -p ./",
        "postinstall": "node ./node_modules/vscode/bin/install",
        "test": "npm run compile && node ./node_modules/vscode/bin/test"
    },
    "devDependencies": {
        "typescript": "^2.6.1",
        "vscode": "^1.1.21",
        "@types/node": "^8.10.25",
        "@types/mocha": "^2.2.42"
    },
    "repository": {
        "type": "git",
        "url": "https://bitbucket.org/dfgtech/cli-web-act1-vscode-extension.git"
    }
}
```

### __2.__ __Ejecución de la extensión__

#### _Iniciar el modo Depuración_

Para ejecutar la extensión nos ubicamos en el menú `Debug` de la barra de tareas de Visual Studio Code y seleccionamos la opción `Start Debugging`, alternativamente podemos pulsar `F5` y obtendremos el mismo resultado.

Inmediatamente después de empezar la depuración se presenta la barra inferior del editor en color tomate como se observa a continuación:

![Barra Inferior](images/footerDebug.PNG)

Adicional a esto se levanta una nueva ventana popup del mismo editor con nuestra extensión precargada y lista para ser probada, como se presenta en la siguiente imagen:

![Ventana Popup](images/popupWindow.PNG)

#### _Abrir cualquier archivo de texto para la prueba_

Con la opción `Open File...` del menú, abrimos un archivo de texto existente y seleccionamos todo el texto que contiene el archivo. A continuación se muestran algunas capturas de pantalla con la secuencia de los pasos antes mencionados.

![Abrir Archivo de Texto](images/openTextFile.PNG)

![Selección del Texto](images/textSelection.PNG)

#### _Iniciar la prueba_

Ejecutar la siguiente secuencia de teclas `Ctrl + Shift + P`. Se abrirá el visor de comandos (command palette), consecutivamente se busca la extensión ingresando el nombre previamente definido en el archivo `package.json`. 

![Búsqueda de extensión](images/searchExtension.PNG)

Al encontrarlo se procede a ejecutarlo y de forma inmediata arrancará la extensión. Si se siguieron de forma exacta los pasos anteriores no debería presentarse ningún problema, lo esperado según la programación que tiene la extensión es poder ver un prompt que nos pida ingresar el número de líneas que deseamos contar antes de insertar una línea en blanco entre las líneas.

![Ingresar número de líneas](images/lineNumber.PNG)

Al ingresar un número entero y pulsar `Enter`, podremos observar como la extensión realiza su trabajo, el resultado se presenta a continuación:

![Resultado de la ejecución](images/extensionResult.PNG)

### __3.__ __Conclusiones__

* Visual Studio Code es un editor de texto muy poderoso y tiene la posibilidad de ampliar su funcionalidad a través de extensiones o plugins.
* Typescript es un lenguaje de programación basado en javascript pero es mucho más robusto y seguro al momento de crear ciertas soluciones de software en el cliente web.
* Es importante conocer bien el potencial que poseen las tecnologías web y el uso que se les puede dar al momento de construir software.
* Un código bien comentado habla por si mismo, ayuda a que este pueda ser fácilmente mantenido y entendido por otros programadores.
* Markdown es una poderosa herramienta al momento de documentar un software y describir su funcionalidad. 